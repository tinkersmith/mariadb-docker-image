#!/bin/bash
#
# Check if MySQL has been initialized, and create databases \w user if needed.
#
# NOTE: That once a database is initialized, it will not update to changes
# with the MYSQL_DATABASE or MYSQL_USER environment variables. A method to
# signal that these need to be refreshed is being planned.
#
# TODO: Create a method to refreshing or forcing rebuilds of Databases other
# besides folder deletion. Will be important if the DB users or password gets
# updated.
#


if ! [ -d "/run/mysqld" ]; then
	mkdir -p /run/mysqld;
	chown -R mysql:mysql /run/mysqld;
fi

if ! [ -d /var/lib/mysql/mysql ]; then
	echo -e "[i] MySQL data directory not found, creating initial DBs"

	chown -R mysql:mysql /var/lib/mysql

	mysql_install_db --user=mysql > /dev/null
	echo "DROP DATABASE IF EXISTS \`test\`;" > tmp_query.tmp

	if ! [ -z "$MYSQL_ROOT_PASSWORD" ]; then
	cat << EOF >> tmp_query.tmp
USE mysql;
FLUSH PRIVILEGES;
CREATE USER IF NOT EXISTS 'root'@'%';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' identified by "$MYSQL_ROOT_PASSWORD" WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' identified by '' WITH GRANT OPTION;
EOF
	fi

	cat tmp_query.tmp | mysqld --user=mysql --bootstrap --verbose=0
	rm tmp_query.tmp
fi

# Create the databases if one there are any specified.
if [ -f "/usr/share/mysql/.manifests/db-manifest.txt" ]; then

	# Start reading the manifest file one line at a time.
	while IFS='' read -r LINE || [[ -n "$LINE" ]]; do
		IFS=';' read -ra DB_INFO <<< "$LINE"

		if ! [ -z "${DB_INFO[0]}" ]; then
			/scripts/create-db.sh "${DB_INFO[0]}" "${DB_INFO[1]}" "${DB_INFO[2]}"
		fi
	done < "/usr/share/mysql/.manifests/db-manifest.txt"

elif ! [ -z "$MYSQL_DATABASE" ]; then
	/scripts/create-db.sh "$MYSQL_DATABASE" "$MYSQL_USER" "$MYSQL_PASSWORD"
fi

exec /usr/bin/mysqld --user=mysql --log-warnings=1
