FROM alpine:edge

ARG MYSQL_SIZE=medium
ARG DEVELOPMENT_TOOLS=0

RUN apk add --update --no-cache bash mariadb mariadb-client

# Move all configurations on the server so they can be used by
# any containers that use this image.
ADD "conf.d" "/root/conf.d"
ADD "create-db.sh" "/scripts/create-db.sh"

# Move a default configuration into place.
RUN mv "/root/conf.d/${MYSQL_SIZE}.cnf" "/etc/mysql/my.cnf"

# Start the s6 supervisor.
ADD entrypoint.sh entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
