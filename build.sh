#!/bin/bash
#
# Script to build and push a new version of this Docker image.
#

# TODO: Add simple build options to allow building of different environments.
DEV=0
TAG="10.1.24"

if ! [ -z "${DEV}" ] && [ "${DEV}" = "1" ]; then
  TAG="${TAG}-dev"
fi

function main() {
  docker build \
    --squash \
    --build-arg "DEVELOPMENT_TOOLS=${DEV}" \
    --build-arg "MYSQL_SIZE=medium" \
    -t "tinkersmith/mariadb:${TAG}" \
    ./
}

main "$@"
