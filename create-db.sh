#!/bin/bash

MYSQL_DATABASE=$1
MYSQL_USER=$2
MYSQL_PASSWORD=$3

SQL_QUERY=""

# Create the database if a database is specified but not already created.
if ! [ -z "${MYSQL_DATABASE}" ] && ! [ -d "/var/lib/mysql/${MYSQL_DATABASE}" ]; then
	echo -e "\e[93m[i] Creating database: ${MYSQL_DATABASE}\e[0m"
	SQL_QUERY="
CREATE DATABASE IF NOT EXISTS \`${MYSQL_DATABASE}\` CHARACTER SET utf8 COLLATE utf8_general_ci;
"

	# Create user account if information is available.
	if ! [ -z "${MYSQL_USER}" ] && ! [ -z "${MYSQL_PASSWORD}" ]; then
		echo -e "\e[93m[i] Adding user: $MYSQL_USER\e[0m";
		SQL_QUERY="${SQL_QUERY}
FLUSH PRIVILEGES;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON \`${MYSQL_DATABASE}\`.* TO '${MYSQL_USER}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';
"
	fi
fi

if ! [ -z "${SQL_QUERY}" ]; then
	echo "${SQL_QUERY}" | mysqld --user=mysql --bootstrap --verbose=0
fi
